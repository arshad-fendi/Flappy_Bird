// draw_text_outline(x,y,outline,text,color)
  
  var _x, _y, outline, precision, text, dir_inc, dir, dist_inc, dist;
  
  // Argument definitions
  _x = argument0;
  _y = argument1;
  outline = argument2;
  text = argument3;
  textColor = argument4;
  
  // Settings
  dprecision = 8; // Directional precision - the number of directions in which the outline is drawn
  lprecision = 2; // Longitudinal precision - the number of times the text is drawn in each direction; set at (outline_size/font_thickness) for optimal performance and results
  
  dir_inc = 360 / dprecision;
  dist_inc = outline / lprecision;
  
  draw_set_color(c_black);
  for (dir = 0; dir < 360; dir += dir_inc) {
  for (dist = dist_inc; dist <= outline; dist += dist_inc) {
    draw_text(_x+lengthdir_x(dist,dir),_y+lengthdir_y(dist,dir),text);
  }
  }
  
  draw_set_color(textColor);
  draw_text(_x,_y,text);
